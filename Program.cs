﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using IdentityModel.Client;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using static LoginTest.Parameters;

namespace LoginTest
{
    public class Program
    {
        static void Main(string[] args)
        {
            var (argAuthServer, argUsername, argPassword, argClientId, argClientSecret, argScope, argRedirectUri, argGrantType, argIsGenerateRefreshToken) = ParseArguments(args);
            var (authServer, username, password, clientId, clientSecret, scope, redirectUri, grantType, isGenerateRefreshToken) =
                AskForAguments(argAuthServer, argUsername, argPassword, argClientId, argClientSecret, argScope, argRedirectUri, argGrantType, argIsGenerateRefreshToken);

            var authServerUri = new Uri(authServer);
            var disco = Discover(authServerUri);
            List<Cookie> loginCookies = null;

            if (grantType == GrantType.AuthorizationCode || grantType == GrantType.Implicit)
            {
                loginCookies = Login(authServerUri, username, password).ToList();
            }

            var scopes = isGenerateRefreshToken ? $"{scope} offline_access" : scope;

            var authorizationCode = string.Empty;
            if (grantType == GrantType.AuthorizationCode)
            {
                authorizationCode =
                    Authorize(
                        authServerUri,
                        loginCookies,
                        clientId,
                        redirectUri,
                        scopes);
            }


            if (grantType == GrantType.Implicit)
            {
                using (var client = new HttpClient())
                {
                    var result =
                        client.RequestImplicitToken(
                            authServerUri,
                            loginCookies,
                            clientId,
                            redirectUri,
                            scopes);
                    Console.WriteLine(JsonConvert.SerializeObject(result));
                    return;
                }
            }

            var tokenResponse = GetTokens(grantType, disco, clientId, clientSecret, authorizationCode, redirectUri, scopes);
            if (tokenResponse.IsError)
                throw new ApplicationException($"Unable to retrieve tokens [{tokenResponse.Error}]");

            if (isGenerateRefreshToken)
            {
                var refreshTokenResponse = RefreshToken(disco, clientId, clientSecret, scope, tokenResponse.RefreshToken);
                Console.WriteLine(refreshTokenResponse.Json);
            }
            else
            {
                Console.WriteLine(tokenResponse.Json);
            }
        }


        private static DiscoveryResponse Discover(Uri authServer)
        {
            using (var client = new HttpClient())
            {
                var disco = client.GetDiscoveryDocumentAsync(authServer.ToString()).GetAwaiter().GetResult();
                if (disco.IsError)
                    throw new ApplicationException("Unable to discover auth server");
                return disco;
            }
        }

        private static IEnumerable<Cookie> Login(Uri authServer, string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentException("The value cannot be empty", nameof(username));
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentException("The value cannot be empty", nameof(password));

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler) { BaseAddress = authServer })
                {
                    var loginRequest =
                        new HttpRequestMessage(
                            HttpMethod.Post,
                            "/api/account/login");
                    loginRequest.Content =
                        new StringContent(
                            GetLoginParameters(username, password),
                            Encoding.UTF8,
                            "application/json");
                    var loginResponse = client.SendAsync(loginRequest).GetAwaiter().GetResult();

                    var loginMessage = loginResponse.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    if (!string.IsNullOrWhiteSpace(loginMessage))
                        throw new ApplicationException(loginMessage);
                }
            }
            return cookieContainer.GetCookies(authServer).Cast<Cookie>();
        }

        private static string Authorize(Uri authServer, IEnumerable<Cookie> loginCookies, string clientId, string redirectUri, string scope)
        {
            var authorizationCode = string.Empty;
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer, AllowAutoRedirect = false })
            {
                using (var clientCode = new HttpClient(handler) { BaseAddress = authServer })
                {
                    var codeRequest =
                        new HttpRequestMessage(
                            HttpMethod.Get,
                            $"/connect/authorize?response_type=code&scope={scope}&client_id={clientId}&redirect_uri={redirectUri}");
                    foreach (var c in loginCookies)
                    {
                        cookieContainer.Add(authServer, c);
                    }
                    var codeResponse = clientCode.SendAsync(codeRequest).GetAwaiter().GetResult();

                    if (codeResponse.StatusCode != HttpStatusCode.Redirect)
                        throw new ApplicationException("Unable to complete authorization");

                    var queryParams = QueryHelpers.ParseQuery(codeResponse.Headers.Location.Query);
                    return queryParams["code"];
                }
            }
        }

        private static TokenResponse GetTokens(
            GrantType grantType,
            DiscoveryResponse disco,
            string clientId,
            string clientSecret,
            string authorizationCode,
            string redirectUri,
            string scopes)
        {
            using (var client = new HttpClient())
            {
                switch (grantType)
                {
                    case GrantType.AuthorizationCode:
                        return
                            client.RequestAuthorizationCodeTokenAsync(
                                new AuthorizationCodeTokenRequest
                                {
                                    Address = disco.TokenEndpoint,
                                    ClientId = clientId,
                                    ClientSecret = clientSecret,
                                    RedirectUri = redirectUri,
                                    Code = authorizationCode,
                                }).GetAwaiter().GetResult();
                    case GrantType.ClientCredentials:
                        return
                            client.RequestClientCredentialsTokenAsync(
                                new ClientCredentialsTokenRequest
                                {
                                    Address = disco.TokenEndpoint,
                                    ClientId = clientId,
                                    ClientSecret = clientSecret,
                                    Scope = scopes,
                                }).GetAwaiter().GetResult();
                    default:
                        throw new NotSupportedException($"Unsupported grantType [{grantType}]");
                }
            }
        }

        private static TokenResponse RefreshToken(DiscoveryResponse disco, string clientId, string clientSecret, string scope, string refreshToken)
        {
            using (var client = new HttpClient())
            {
                return client.RequestRefreshTokenAsync(
                    new RefreshTokenRequest
                    {
                        Address = disco.TokenEndpoint,
                        ClientId = clientId,
                        ClientSecret = clientSecret,
                        GrantType = "refresh_token",
                        RefreshToken = refreshToken
                    }).GetAwaiter().GetResult();
            }
        }
    }
}
