using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;

namespace LoginTest
{
    public static class ImplicitGrantExtension
    {
        public static TokenResult RequestImplicitToken(
            this HttpClient client,
            Uri authServer,
            IEnumerable<Cookie> loginCookies,
            string clientId,
            string redirectUri,
            string scope)
        {
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer, AllowAutoRedirect = false })
            {
                using (var clientCode = new HttpClient(handler) { BaseAddress = authServer })
                {
                    var request =
                        new HttpRequestMessage(
                            HttpMethod.Get,
                            $"/connect/authorize?response_type=token&scope={scope}&client_id={clientId}&redirect_uri={redirectUri}");
                    foreach (var c in loginCookies)
                    {
                        cookieContainer.Add(authServer, c);
                    }
                    var response = clientCode.SendAsync(request).GetAwaiter().GetResult();

                    Console.WriteLine($"Code: {response.StatusCode}");
                    Console.WriteLine($"Code: {response.Headers.Location.Query}");
                    if (response.StatusCode != HttpStatusCode.Redirect)
                        throw new ApplicationException("Unable to retrieve token using implicit grant");

                    return new TokenResult(response.Headers.Location);
                }
            }
        }
    }
}

