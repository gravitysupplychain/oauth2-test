using System;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace LoginTest
{
    public static class Parameters
    {
        public static string GetLoginParameters(string username, string password)
        {
            var param = (dynamic)new JObject();
            param.Username = username;
            param.Password = password;
            return param.ToString();
        }

        public static (string, string, string, string, string, string, string, string, bool?) ParseArguments(string[] args)
        {
            var authServer = args.ElementAtOrDefault(0);
            var username = args.ElementAtOrDefault(1);
            var password = args.ElementAtOrDefault(2);
            var clientId = args.ElementAtOrDefault(3);
            var clientSecret = args.ElementAtOrDefault(4);
            var scope = args.ElementAtOrDefault(5);
            var redirectUri = args.ElementAtOrDefault(6);
            var grantType = args.ElementAtOrDefault(7);
            bool? isGenereateRefreshToken = null;
            if (args.ElementAtOrDefault(8) != null)
                isGenereateRefreshToken = args.ElementAtOrDefault(8) == "y";

            return (authServer, username, password, clientId, clientSecret, scope, redirectUri, grantType, isGenereateRefreshToken);
        }

        public static (string, string, string, string, string, string, string, GrantType, bool) AskForAguments(
            string authServer,
            string username,
            string password,
            string clientId,
            string clientSecret,
            string scope,
            string redirectUri,
            string inputGrantType,
            bool? isGenereateRefreshToken)
        {
            var grantType =
                string.IsNullOrWhiteSpace(inputGrantType)
                    ? AskForGrantType()
                    : GetGrantTypeFromString(inputGrantType);

            var defaultAuthServer = "https://uat.gravitysupplychain.com";
            if (string.IsNullOrWhiteSpace(authServer))
            {
                Console.WriteLine($"\nServer? (example: {defaultAuthServer})");
                authServer = Console.ReadLine().Trim();
            }

            if ((grantType == GrantType.AuthorizationCode || grantType == GrantType.Implicit) &&
                string.IsNullOrWhiteSpace(username))
            {
                Console.WriteLine("\nUsername?");
                username = Console.ReadLine().Trim();
            }

            if ((grantType == GrantType.AuthorizationCode || grantType == GrantType.Implicit) &&
                string.IsNullOrWhiteSpace(password))
            {
                Console.WriteLine("\nPassword?");
                password = Console.ReadLine().Trim();
            }

            if (string.IsNullOrWhiteSpace(clientId))
            {
                Console.WriteLine("\nclient_id?");
                clientId = Console.ReadLine().Trim();
            }

            if ((grantType == GrantType.AuthorizationCode || grantType == GrantType.ClientCredentials) &&
                string.IsNullOrWhiteSpace(clientSecret))
            {
                Console.WriteLine("\nclient_secret?");
                clientSecret = Console.ReadLine().Trim();
            }

            if (string.IsNullOrWhiteSpace(scope))
            {
                Console.WriteLine("\nscope? (example: api,datascience-api)");
                scope = Console.ReadLine().Trim();
            }

            if ((grantType == GrantType.AuthorizationCode || grantType == GrantType.Implicit) &&
                string.IsNullOrWhiteSpace(redirectUri))
            {
                Console.WriteLine("\nredirect_uri? (example: http://localhost:3048/help/oauth2-redirect.html");
                redirectUri = Console.ReadLine().Trim();
            }

            isGenereateRefreshToken =
                grantType == GrantType.AuthorizationCode || grantType == GrantType.ClientCredentials
                    ? (isGenereateRefreshToken.HasValue ? isGenereateRefreshToken.Value : AskForRefreshToken())
                    : false;

            return (authServer, username, password, clientId, clientSecret, scope, redirectUri, grantType, isGenereateRefreshToken.Value);
        }

        private static bool AskForRefreshToken()
        {
            Console.WriteLine("\nGenerate refresh token? (y/n)");
            return Console.ReadLine().Trim() == "y";
        }

        private static GrantType AskForGrantType()
        {
            Console.WriteLine("\nWhich grant type (Authorization Code, Implicit or Client Credentials)? (a/i/c)");
            var answer = Console.ReadLine().Trim();
            return GetGrantTypeFromString(answer);
        }

        private static GrantType GetGrantTypeFromString(string input)
        {
            switch (input)
            {
                case "a":
                    return GrantType.AuthorizationCode;
                case "i":
                    return GrantType.Implicit;
                case "c":
                    return GrantType.ClientCredentials;
                default:
                    throw new NotSupportedException($"Unsupported grant type [{input}].");
            }
        }

        private const string FormatMessage = "Format: dotnet run <auth-server> <username> <password> <client_id> <client_secret> <scope> <redirect_uri>";

        public enum GrantType
        {
            AuthorizationCode,
            Implicit,
            ClientCredentials
        }
    }
}
