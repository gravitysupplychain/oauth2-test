using System;
using Microsoft.AspNetCore.WebUtilities;

namespace LoginTest
{
    public class TokenResult
    {
        public TokenResult(Uri redirectUri)
        {
            var redirectUrl = redirectUri.ToString();
            if (string.IsNullOrWhiteSpace(redirectUrl))
                throw new ArgumentException("The value cannot be empty or null", nameof(redirectUrl));
            var hashIndex = redirectUrl.IndexOf('#');
            if (hashIndex < 0)
                throw new ApplicationException("Invalid redirect URL");
            var queryString = redirectUrl.Substring(hashIndex + 1);
            var dict = QueryHelpers.ParseQuery(queryString);

            AccessToken = dict["access_token"];
            TokenType = dict["token_type"];
            ExpiresIn = int.Parse(dict["expires_in"]);
        }

        public string AccessToken { get; set; }
        public string TokenType { get; set; }
        public int ExpiresIn { get; set; }
    }
}
