## Application

### Requirements

1. .NET Core 2.1 SDK (or later) has been installed

### To download

```sh
git clone https://bitbucket.org/gravitysupplychain/oauth2-test
cd oauth2-test
```

### To run

```sh
dotnet run 
```

## To test without using this application and using only cURL

Assuming the following environment variables has been set and
[jq](https://stedolan.github.io/jq/) has been installed.

```sh
DOMAIN=your-domain.com
USERNAME=your_username
PASSWORD=your_password
REDIRECT_URI=https://your-domain.com/callback
CLIENT_ID=your_client_id
CLIENT_SECRET=your_client_name
AUTH_ENDPOINT=$(curl -s https://$DOMAIN/.well-known/openid-configuration | jq -r '.authorization_endpoint')
TOKEN_ENDPOINT=$(curl -s https://$DOMAIN/.well-known/openid-configuration | jq -r '.token_endpoint')
```

Login to retrieve cookies.

```sh
curl -i -X POST -H "Content-Type: application/json" -d "{ \"Username\": \"${USERNAME}\", \"Password\": \"${PASSWORD}\" }" -c cookies.txt https://${DOMAIN}/api/account/login
```

Using the two cookies in `cookies.txt` to get an authorization code.

```sh
CODE=$(curl -sSk -b cookies.txt -d "response_type=code&client_id=${CLIENT_ID}&scope=api&redirect_uri=${REDIRECT_URI}" -D - -o /dev/null ${AUTH_ENDPOINT} | grep -i location: | grep -vi error | cut -d ' ' -f 2 | cut -d '?' -f 2 | cut -d '&' -f 1 | cut -d '=' -f 2)
```

To retrieve an access token,

```sh
TOKEN=$(curl -sk -d "grant_type=authorization_code&code=${CODE}&redirect_uri=${REDIRECT_URI}" --user ${CLIENT_ID}:${CLIENT_SECRET} ${TOKEN_ENDPOINT} | jq -r .access_token)
echo $TOKEN
```
